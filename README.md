# Four-Vector Potential

EM_Project_2020 contains historical and background information about the four-vector potential, 
and analytical derivation of a proof that the vector potential is a four
vector, and a derivation of the homogeneous Maxwell equation using the four
vector potential. Additionally, it contains various numerical testing of the four-vector potential,
depicting and simulating the resulting electric and magnetic fields.

Python Code contains the code and methods used for the numerical testing reported in EM_Project_2020.

Animations contains the referenced animations in EM_Project_2020, that simulate the potential, electric field,
and the magnetic field.


# Contributors
* Anthony Cabanillas <acabanil@buffalo.edu>
* James Carter <jrcarter@buffalo.edu>
* Lauren Kim <lhkim@buffalo.edu>
* Benjamin Mannix <brmannix@buffalo.edu>
* Eric Niblock <ericnibl@buffalo.edu>